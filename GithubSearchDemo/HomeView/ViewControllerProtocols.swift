//
//  ViewControllerProtocols.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation
import CoreGraphics

public protocol userListControllerCallBack {
    func reloadTableView()
}

///View model methods and getter/setters.
public protocol viewControllerConfigurables: AnyObject {
    var delegate: userListControllerCallBack? {get set}
    func numberOfRows(inSection section: Int) -> Int
    func cellViewModel(at IndexPath: IndexPath) -> UserListTableViewCellConfigurables?
    func didSelectRow(at indexPath: IndexPath) -> Any?
    func performUserFetch(withQueryString: String)
    func clearArray()
}

public protocol UserListTableViewCellConfigurables: AnyObject {
    var userName: String? {get}
    var userImageURL: String? {get}
    var userRepoCount: String? {get}
}

final class UserListTableViewCellModel: UserListTableViewCellConfigurables {
    var userName: String?
       var userImageURL: String?
       var userRepoCount: String?
    
    init(userName: String, userImageURL: String, userRepoCount: String) {
        self.userName = userName
        self.userImageURL = userImageURL
        self.userRepoCount = userRepoCount
    }
}
