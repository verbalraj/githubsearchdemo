//
//  HomeModel.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/20/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation

public struct UserData {
    var name: String?
    var forkCount: Int?
    var owner: Owner?
    init(json: [String:Any]) {
        self.name = json["name"] as? String
        self.forkCount = json["forks"] as? Int
        if let owner = json["owner"] as? [String:Any] {
            self.owner = Owner(json: owner)
            
        }
    }
}
public struct Owner {
    var avataarURL: String?
    var login: String?
    init(json: [String:Any]) {
        self.avataarURL = json["avatar_url"] as? String
        self.login = json["login"] as? String
    }
}
