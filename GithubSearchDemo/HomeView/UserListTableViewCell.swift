//
//  UserListTableViewCell.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit

class UserListTableViewCell: UITableViewCell {
    @IBOutlet weak var userAvtarImageView: CustomImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userRepoCountLabel: UILabel!
    
    var cellViewModel: UserListTableViewCellConfigurables! {
        didSet {
            if let name = cellViewModel.userName, let imageURL = cellViewModel.userImageURL, let repoCount = cellViewModel.userRepoCount {
                userNameLabel.text = "Name: \(name)"
                userRepoCountLabel.text = "Fork: \(repoCount)"
                userAvtarImageView.loadImageUsingUrlString(urlString: imageURL)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    class func reUseID() -> String {
        return String(describing: self)
    }
    
}
