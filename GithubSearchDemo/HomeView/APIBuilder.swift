//
//  APIBuilder.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation

enum APIUrls {
    // Endpoints
    static let githubUserSearchURL = "https://api.github.com/search/repositories?q="
    static let profileInforURL = "https://api.github.com/users/" ///Append user name here.
    static let repoRatingURL = "/repos" ///username/reponame
}
///Set Nav bar title used in app as per controller's name - I think new developer can frequently learn that pattern within team.
enum InAppNavigationBarTitle {
    //If possible for ease please use class name for the title of nav bar
    static let viewControllerNavBarTitle = "GitHub Search Demo"
    static let detailViewControllerNavBarTitle = "Profile Detail"
}

enum InAppString {
    //If possible for ease please use class name for the title of nav bar
    static let searchBarPlaceHolder = "Search for a user on github"
    static let detailViewControllerNavBarTitle = "Profile Detail"
    static let loadingIndicatorTitle = "Loading..."
}

///Tried to give name of storyboard as they are.
enum InAppStoryBoardName {
    static let detailView = "DetailView"
    static let detailViewIdentifier = "GithubUserDetailViewController"
}
