//
//  AsyncImageDownloader.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit
let imageCache = NSCache<NSString, UIImage>()
class CustomImageView: UIImageView {
    var imageUrlString: String?
    
    func loadImageUsingUrlString(urlString: String) {
        imageUrlString = urlString
        guard let url = URL(string: urlString) else { return }
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            DispatchQueue.main.async {
                self.image = nil
                self.image = imageFromCache
                return
            }
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, respones, error) in
            if error != nil {
                print(error ?? "")
                return
            }
            DispatchQueue.main.async {
                guard let imageToCache = UIImage(data: data!) else { return }
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                imageCache.setObject(imageToCache, forKey: urlString as NSString)
            }
        }).resume()
    }
    
}
