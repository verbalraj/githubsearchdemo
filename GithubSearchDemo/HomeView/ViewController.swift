//
//  ViewController.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,userListControllerCallBack {
    @IBOutlet weak var githubUserListTableView: UITableView!
    @IBOutlet weak var searhBar: UISearchBar!
    var searchTimer = Timer()
    
    var viewModel: viewControllerConfigurables! {
        didSet {
            viewModel?.delegate = self
        }
    }
    
    private func setUpUI() {
        self.navigationItem.title = InAppNavigationBarTitle.viewControllerNavBarTitle
        searhBar.placeholder = InAppString.searchBarPlaceHolder
        searhBar.showsCancelButton = true
    }
    
    private func setupDelegate(){
        self.viewModel = ViewControllerViewModel()
        githubUserListTableView.delegate = self
        githubUserListTableView.dataSource = self
        searhBar.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        registerCell()
        setupDelegate()
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.githubUserListTableView.reloadData()
        }
    }
    
    final func registerCell(){
        self.githubUserListTableView.register(UINib(nibName: UserListTableViewCell.reUseID(), bundle: nil), forCellReuseIdentifier: UserListTableViewCell.reUseID())
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserListTableViewCell.reUseID(), for: indexPath) as? UserListTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = (indexPath.row % 2 == 0) ? UIColor.green.withAlphaComponent(0.05) : UIColor.white
        cell.tag = indexPath.row
        cell.cellViewModel = self.viewModel.cellViewModel(at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC = UIStoryboard(name: InAppStoryBoardName.detailView, bundle: nil).instantiateViewController(identifier: InAppStoryBoardName.detailViewIdentifier) as? GithubUserDetailViewController {
            detailVC.viewModel = DetailViewViewModel()
            detailVC.viewModel?.delegate = detailVC
            detailVC.viewModel?.userName = self.viewModel.didSelectRow(at: indexPath) as? String
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTimer.invalidate()
       searchTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(0.30), repeats: false, block: { _ in
            DispatchQueue.main.async {
                self.performSearch(for: searchText)
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func performSearch(for searchText: String) {
        guard !searchText.isEmpty else {
            searhBar.endEditing(true)
            self.viewModel.clearArray()
            reloadTableView()
            return
        }
        self.viewModel.performUserFetch(withQueryString: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}

