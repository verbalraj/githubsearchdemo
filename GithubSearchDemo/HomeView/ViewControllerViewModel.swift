//
//  ViewControllerViewModel.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation
import CoreGraphics

final class ViewControllerViewModel: viewControllerConfigurables {
    var delegate: userListControllerCallBack?
    var userDataArray: [UserData] = [] {
        didSet {
            delegate?.reloadTableView()
        }
    }
    
    func clearArray() {
        userDataArray.removeAll()
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return userDataArray.count
    }
    
    func cellViewModel(at IndexPath: IndexPath) -> UserListTableViewCellConfigurables? {
        if let name = userDataArray[IndexPath.row].name, let imageURL = userDataArray[IndexPath.row].owner?.avataarURL, let repoCount = userDataArray[IndexPath.row].forkCount {
            return UserListTableViewCellModel(userName: name, userImageURL: imageURL, userRepoCount: String(repoCount))
        }
        return UserListTableViewCellModel(userName: "", userImageURL: "", userRepoCount: "")
    }
    
    func didSelectRow(at indexPath: IndexPath) -> Any? {
        return userDataArray[indexPath.row].owner?.login
    }
    
    func performUserFetch(withQueryString: String) {
        LoadingIndicator.show(InAppString.loadingIndicatorTitle)
        guard let url = URL(string: "\(APIUrls.githubUserSearchURL)\(withQueryString)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        print(request)
        URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                print("API didn't respond well.")
                LoadingIndicator.hide()
                return
            }
            if let json = data {
                do {
                    let values = try JSONSerialization.jsonObject(with: json, options: .mutableLeaves) as? [String:Any]
                    let userListArrray = values?["items"] as? Array<[String:Any]>
                    self?.clearArray()
                    for (index,_ ) in  userListArrray!.enumerated() {
                        if let final = userListArrray? [index]  {
                            self?.userDataArray.append(UserData.init(json: final))
                        }
                    }
                    
                } catch let jsonErr {
                    print("Error decoding json:", jsonErr)
                }
            }
            LoadingIndicator.hide()
        }.resume()
    }
    
}
