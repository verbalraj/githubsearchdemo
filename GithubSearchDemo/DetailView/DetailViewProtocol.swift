//
//  DetailViewProtocol.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/20/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation
import CoreGraphics

public protocol detailViewControllerCallBack {
    func reloadTableView()
    func setUpDetailProfileInfo(with:userDetailData)
}

public protocol DetailViewControllerConfigurables: AnyObject {
    var userName: String? {get set}
    var delegate: detailViewControllerCallBack? {get set}
    func numberOfRows(inSection section: Int) -> Int
    func cellViewModel(at IndexPath: IndexPath) -> DetailViewTableViewCellConfigurables?
    func performUserDetailsFetch()
}

public protocol DetailViewTableViewCellConfigurables: AnyObject {
    var repoName: String? {get}
    var forkCount: String? {get}
    var starCount: String? {get}
}


final class DetailViewTableViewCellModel: DetailViewTableViewCellConfigurables {
    var repoName: String?
    var forkCount: String?
    var starCount: String?
    
    init(repoName: String, forkCount: String, starCount: String) {
        self.repoName = repoName
        self.forkCount = forkCount
        self.starCount = starCount
    }
}
