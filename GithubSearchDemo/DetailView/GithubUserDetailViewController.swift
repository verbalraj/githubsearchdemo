//
//  GithubUserDetailViewController.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit

final class GithubUserDetailViewController: UIViewController, detailViewControllerCallBack, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var avtarImageView: CustomImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userLocation: UILabel!
    @IBOutlet weak var userJoinDate: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var repoListTableView: UITableView!
    var viewModel: DetailViewControllerConfigurables?
    
    private func setupDelegate(){
          repoListTableView.delegate = self
          repoListTableView.dataSource = self
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setupDelegate()
        self.viewModel?.performUserDetailsFetch()
    }
    
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.repoListTableView.reloadData()
        }
    }
    
    func covertTime(withString: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "MMM dd,yyyy"
        guard let date = dateFormatterGet.date(from: withString) else {
            return ""
        }
        return dateFormatterString.string(from: date)
    }
    
    func setUpDetailProfileInfo(with: userDetailData) {
      
        if let name = with.userName, let email = with.email, let location = with.location, let follower = with.followCount, let following = with.followingCount, let imageURL = with.avatarImage, let date = with.joinDate {
              
            DispatchQueue.main.async {
                self.userName.text = "User: \(name)"
                self.userEmail.text = "Email: \(email)"
                self.userLocation.text = "Location: \(location)"
                self.userJoinDate.text = "Join date: \(self.covertTime(withString: date))"
                self.followerCount.text = "\(follower) Follower"
                self.followingCount.text = "\(following) Following"
            }
            avtarImageView.loadImageUsingUrlString(urlString: imageURL)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfRows(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailViewTableViewCell.reUseID(), for: indexPath) as? DetailViewTableViewCell else {
                   return UITableViewCell()
               }
        cell.cellViewModel = self.viewModel?.cellViewModel(at: indexPath)
        return cell
    }
    
    final func registerCell(){
        self.repoListTableView.register(UINib(nibName: DetailViewTableViewCell.reUseID(), bundle: nil), forCellReuseIdentifier: DetailViewTableViewCell.reUseID())
    }
    
}
