//
//  DetailViewViewModel.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/20/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
// test commit from terminal 

import Foundation

final class DetailViewViewModel: DetailViewControllerConfigurables {
    
    var delegate: detailViewControllerCallBack?
    var userName: String?
    var userRepoDetailArray: [userRepoDetail] = []
    
    func numberOfRows(inSection section: Int) -> Int {
        return userRepoDetailArray.count
    }
    
    func cellViewModel(at IndexPath: IndexPath) -> DetailViewTableViewCellConfigurables? {
        if let name = userRepoDetailArray[IndexPath.row].repoName, let fork = userRepoDetailArray[IndexPath.row].forkCount, let star = userRepoDetailArray[IndexPath.row].starCount {
            return DetailViewTableViewCellModel(repoName: name, forkCount: String(fork), starCount: String(star))
        }
        return DetailViewTableViewCellModel(repoName: "", forkCount: "", starCount: "")
    }
    
    func performUserDetailsFetch() {
        DispatchQueue.global(qos: .background).async {
            self.performUserRepoCountDetails()
        }
        LoadingIndicator.show(InAppString.loadingIndicatorTitle)
        guard let user = userName, let url = URL(string: "\(APIUrls.profileInforURL)\(user)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        print(request)
        URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                print("API didn't respond well.")
                LoadingIndicator.hide()
                return
            }
            if let json = data {
                do {
                    let values = try JSONSerialization.jsonObject(with: json, options: .mutableLeaves) as? [String:Any]
                    if let json = values {
                        self?.delegate?.setUpDetailProfileInfo(with: userDetailData(withJson: json))
                    }
                } catch let jsonErr {
                    print("Error decoding json:", jsonErr)
                }
            }
            LoadingIndicator.hide()
        }.resume()
    }
    
    func performUserRepoCountDetails() {
        guard let user = userName, let url = URL(string: "\(APIUrls.profileInforURL)\(user)\(APIUrls.repoRatingURL)") else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        print(request)
        URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                print("API didn't respond well.")
                return
            }
            if let json = data {
                do {
                    let values = try JSONSerialization.jsonObject(with: json, options: .mutableLeaves) as? Array<[String:Any]>
                    for (index,_ ) in  values!.enumerated() {
                        if let final = values? [index]  {
                            self?.userRepoDetailArray.append(userRepoDetail(withJson: final))
                        }
                    }
                    self?.delegate?.reloadTableView()
                } catch let jsonErr {
                    print("Error decoding json:", jsonErr)
                }
            }
        }.resume()
    }
}
