//
//  DetailViewTableViewCell.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/20/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit

class DetailViewTableViewCell: UITableViewCell {
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var forkCountlabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    var cellViewModel: DetailViewTableViewCellConfigurables! {
        didSet {
            if let name = cellViewModel.repoName, let fork = cellViewModel.forkCount, let star = cellViewModel.starCount {
                repoNameLabel.text = name
                forkCountlabel.text = "\(fork) Fork"
                starCountLabel.text = "\(star) Star"
            }
        }
    }
    class func reUseID() -> String {
        return String(describing: self)
    }
    
}
