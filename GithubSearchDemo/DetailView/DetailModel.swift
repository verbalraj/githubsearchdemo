//
//  DetailModel.swift
//  GithubSearchDemo
//
//  Created by DataCore Inc  on 4/20/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation

public struct userDetailData {
    var userName: String?
    var email: String?
    var location: String?
    var joinDate: String?
    var followCount: Int?
    var followingCount: Int?
    var avatarImage: String?
    
    init(withJson: [String:Any]) {
        self.userName = withJson["login"] as? String ?? ""
        self.email = withJson["email"] as? String ?? ""
        self.location = withJson["location"] as? String ?? ""
        self.joinDate = withJson["created_at"] as? String ?? ""
        self.followCount = withJson["followers"] as? Int ?? 0
        self.followingCount = withJson["following"] as? Int ?? 0
        self.avatarImage = withJson["avatar_url"] as? String ?? ""
    }
}


public struct userRepoDetail {
    var repoName: String?
    var forkCount: Int?
    var starCount: Int?
    
    init(withJson: [String:Any]) {
        self.repoName = withJson["name"] as? String
        self.forkCount = withJson["forks_count"] as? Int ?? 0
        self.starCount = withJson["stargazers_count"] as? Int ?? 0
    }
}
